<?php

declare(strict_types=1);

namespace Shipping;

class OrderService
{

    protected $products = [];

    private $shippingCalculateFactory;


    public function __construct(ShippingCalculateInterface $shippingCalculateFactory)
    {
        $this->shippingCalculateFactory = $shippingCalculateFactory;
    }

    public function getGrossPrice()
    {
        $total = 0;        
        foreach ($this->products as $product) {
            $shippingCalculateType = $this->shippingCalculateFactory->getCalculateType($product);
            $shippingFee = $shippingCalculateType->getShippingFee();
            $itemPrice = $product->getAmazonPrice() + $shippingFee;
            $total += $itemPrice;
        }
        return $total;
    }

    /**
     * Get the value of products
     */ 
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set the value of products
     *
     * @return  self
     */ 
    public function setProducts(array $products)
    {
        $this->products = $products;

        return $this;
    }
}
