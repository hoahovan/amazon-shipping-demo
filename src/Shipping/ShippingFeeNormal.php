<?php

declare(strict_types=1);

namespace Shipping;

class ShippingFeeNormal extends ShippingFeeAbstract
{
    public function getShippingFee(): float
    {
        $feeByWeight = $this->getFeeByWeight();
        $feeByDimension = $this->getFeeByDimension();
        return max($feeByWeight, $feeByDimension);
    }
}
