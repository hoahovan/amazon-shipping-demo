<?php

declare(strict_types=1);

namespace Shipping;

class Product
{

    protected float $amazonPrice;

    protected float $productWeight;

    protected float $width;

    protected float $height;

    protected float $depth;


    public function __construct(array $data)
    {
        if (
            !isset($data['amazon_price']) || !isset($data['product_weight']) ||
            !isset($data['width']) || !isset($data['height']) || !isset($data['depth'])
        ) {
            throw new \Exception('Product data not correct!');
        }
        $this->amazonPrice = (float) $data['amazon_price'];
        $this->productWeight = (float) $data['product_weight'];
        $this->width = (float) $data['width'];
        $this->height = (float) $data['height'];
        $this->depth = (float) $data['depth'];
    }

    /**
     * Get the value of amazonPrice
     */
    public function getAmazonPrice()
    {
        return $this->amazonPrice;
    }

    /**
     * Get the value of productWeight
     */
    public function getProductWeight()
    {
        return $this->productWeight;
    }

    /**
     * Get the value of width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Get the value of height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get the value of depth
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set the value of amazonPrice
     *
     * @return  self
     */ 
    public function setAmazonPrice($amazonPrice)
    {
        $this->amazonPrice = $amazonPrice;

        return $this;
    }

    /**
     * Set the value of productWeight
     *
     * @return  self
     */ 
    public function setProductWeight($productWeight)
    {
        $this->productWeight = $productWeight;

        return $this;
    }

    /**
     * Set the value of width
     *
     * @return  self
     */ 
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Set the value of height
     *
     * @return  self
     */ 
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Set the value of depth
     *
     * @return  self
     */ 
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }
}
