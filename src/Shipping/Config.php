<?php

declare(strict_types=1);

namespace Shipping;

class Config
{

    private static $_instance = null;

    private $config;

    private function __construct()
    {
        $this->config = require_once(dirname(__FILE__) . '/../../core/settings.php');
    }

    /**
     * Returns the instance.
     * 
     * @static
     * @return Config
     */
    public static function getInstance(): Config
    {
        if (self::$_instance == null) {
            self::$_instance = new Self;
        }

        return self::$_instance;
    }

    /**
     * Get a config item.
     * 
     * @param $name
     *
     */
    public function get(string $name)
    {
        $config = $this->config;
        if (!isset($config[$name])) {
            throw new \Exception('Config variable not exist: ' . $name);
        }
        return $config[$name];
    }

    public function __destruct()
    {
        self::$_instance = null;
    }
}
