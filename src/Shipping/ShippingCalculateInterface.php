<?php

declare(strict_types=1);

namespace Shipping;

interface ShippingCalculateInterface
{

    public function getCalculateType(Product $product): ShippingFeeInterface;
}
