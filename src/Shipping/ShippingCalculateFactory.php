<?php

declare(strict_types=1);

namespace Shipping;

class ShippingCalculateFactory implements ShippingCalculateInterface
{

    public function getCalculateType(Product $product): ShippingFeeInterface
    {
        // if ($product->getWidth() >= 200) {
        //     return new ShippingFeePaytype($product);
        // }
        return new ShippingFeeNormal($product);
    }
}
