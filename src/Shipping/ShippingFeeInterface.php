<?php

declare(strict_types=1);

namespace Shipping;

interface ShippingFeeInterface
{
    public function getShippingFee(): float;
}
