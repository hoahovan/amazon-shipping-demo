<?php

declare(strict_types=1);

namespace Shipping;

abstract class ShippingFeeAbstract implements ShippingFeeInterface
{

    /**
     * @var Product
     */
    protected Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getFeeByWeight(): float
    {
        $weightCoefficient = Config::getInstance()->get('weight_coefficient');
        return $this->product->getProductWeight() * $weightCoefficient;
    }

    public function getFeeByDimension(): float
    {
        $dimensionCoefficient = Config::getInstance()->get('dimension_coefficient');
        return $this->product->getWidth() * $this->product->getHeight() * $this->product->getDepth() * $dimensionCoefficient;
    }

    /**
     * Get the value of product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
}
