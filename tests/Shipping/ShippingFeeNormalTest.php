<?php

namespace Tests\Shipping;

use Shipping\Product;
use Shipping\ShippingFeeNormal;
use Shipping\Config;

class ShippingFeeNormalTest extends BaseTestCase
{

    private $product;

    protected function setUp(): void
    {
        parent::setUp();
        $this->product = new Product([
            'amazon_price' => 2023.44,
            'product_weight' => 3000,
            'width' => 200,
            'height' => 100,
            'depth' => 3
        ]);
    }

    public function testGetFeeByWeight()
    {
        $normalShiping = new ShippingFeeNormal($this->product);
        $weightCoefficient = Config::getInstance()->get('weight_coefficient');
        $this->assertEquals(3000 * $weightCoefficient, $normalShiping->getFeeByWeight());
    }

    public function testGetFeeByDimension()
    {
        $normalShiping = new ShippingFeeNormal($this->product);
        $dimensionCoefficient = Config::getInstance()->get('dimension_coefficient');
        $this->assertEquals(200 * 100 * 3 * $dimensionCoefficient, $normalShiping->getFeeByDimension());
    }

    public function testGetShippingFee()
    {
        $normalShiping = new ShippingFeeNormal($this->product);
        $dimensionCoefficient = Config::getInstance()->get('dimension_coefficient');
        $weightCoefficient = Config::getInstance()->get('weight_coefficient');
        $fee = max(200 * 100 * 3 * $dimensionCoefficient, 3000 * $weightCoefficient);
        $this->assertEquals($fee, $normalShiping->getShippingFee());
    }
}
