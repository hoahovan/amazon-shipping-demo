<?php

namespace Tests\Shipping;

use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{

    protected $containerBuilder;

    protected function setUp(): void
    {
        require(dirname(__FILE__) . '/../../core/bootstrap.php');
        $this->containerBuilder = $containerBuilder;
    }
}
