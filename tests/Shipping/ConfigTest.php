<?php

namespace Tests\Shipping;

use PHPUnit\Framework\TestCase;
use Shipping\Config;

class ConfigTest extends TestCase
{

    public function testGetInstance()
    {
        $instance = Config::getInstance();
        $instance2 = Config::getInstance();
        $this->assertEquals($instance, $instance2);

        $this->assertNotEmpty($instance->get('weight_coefficient'));
        $this->assertNotEmpty($instance->get('dimension_coefficient'));
    }
}
