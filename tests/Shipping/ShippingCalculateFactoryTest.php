<?php

namespace Tests\Shipping;

use Shipping\Product;
use Shipping\ShippingCalculateFactory;

class ShippingCalculateFactoryTest extends BaseTestCase
{

    public function testDependencyInjection()
    {
        $orderService = $this->containerBuilder->get('shipping_calculate.service');
        $this->assertInstanceOf(ShippingCalculateFactory::class, $orderService);
    }


    public function testGetCalculateType()
    {
        $shippingCalculateService = $this->containerBuilder->get('shipping_calculate.service');
        $newProduct = new Product([
            'amazon_price' => 2023.44,
            'product_weight' => 3000,
            'width' => 200,
            'height' => 100,
            'depth' => 3
        ]);
        $calculateType = $shippingCalculateService->getCalculateType($newProduct);
        $this->assertNotNull($calculateType);
    }
}
