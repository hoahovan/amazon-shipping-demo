<?php

namespace Tests\Shipping;

use Shipping\OrderService;

class OrderServiceTest extends BaseTestCase
{

    public function testDependencyInjection()
    {
        $orderService = $this->containerBuilder->get('order.service');
        $this->assertInstanceOf(OrderService::class, $orderService);
    }

    public function testGetGrossPrice()
    {
        $products = [
            new \Shipping\Product([
                'amazon_price' => 2022,
                'product_weight' => 3000,
                'width' => 200,
                'height' => 30,
                'depth' => 3
            ]),
            new \Shipping\Product([
                'amazon_price' => 2023,
                'product_weight' => 1000,
                'width' => 200,
                'height' => 30,
                'depth' => 3
            ])
        ];
        $orderService = $this->containerBuilder->get('order.service');
        $orderService->setProducts($products);
        $totalPrice = $orderService->getGrossPrice();
        $this->assertNotNull($totalPrice);

        // Change product price
        $oldPrice = $products[0]->getAmazonPrice();
        $products[0]->setAmazonPrice($oldPrice + 1000);
        $orderService->setProducts($products);
        $newTotalPrice = $orderService->getGrossPrice();
        $this->assertSame($totalPrice + 1000, $newTotalPrice);
    }
}
