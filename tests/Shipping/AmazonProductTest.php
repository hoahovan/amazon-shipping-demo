<?php

namespace Tests\Shipping;

use PHPUnit\Framework\TestCase;
use Shipping\Product;

class ProductTest extends TestCase
{

    public function testConstructor()
    {
        $newProduct = new Product([
            'amazon_price' => 2023.44,
            'product_weight' => 3000,
            'width' => 200,
            'height' => 100,
            'depth' => 3
        ]);
        $this->assertInstanceOf(Product::class, $newProduct);
    }
}
