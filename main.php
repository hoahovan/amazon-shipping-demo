<?php

declare(strict_types=1);

require_once(dirname(__FILE__) . '/core/bootstrap.php');

try {
    $products = [
        new \Shipping\Product([
            'amazon_price' => 2022,
            'product_weight' => 3000,
            'width' => 30,
            'height' => 10,
            'depth' => 10
        ]),
        new \Shipping\Product([
            'amazon_price' => 2023,
            'product_weight' => 3000,
            'width' => 30,
            'height' => 10,
            'depth' => 10
        ])
    ];
    $orderService = $containerBuilder->get('order.service');
    $orderService->setProducts($products);
    print($orderService->getGrossPrice());
} catch (\Exception $e) {
    print($e->getMessage());
}
